CC		= gcc 
WARNIGNS	= -Wall
OPTIMIZE	= -O3
DEBUG		= -g
LINK		= -lgsl -lgslcblas -lm
FLAGS		= $(WARNIGNS) $(OPTIMIZE) $(DEBUG)

OBJ 		= obj/gconf.o obj/shells.o obj/conf_energy.o
OBJ_v 		= obj/value.o obj/md.o obj/mc.o obj/energy.o obj/fenergy.o obj/conf.o obj/delta.o obj/orient.o obj/trajs.o obj/temp.o obj/penergy.o obj/rho.o


all :	 	$(OBJ)	$(OBJ_v) value1 gconf shells conf_energy
		
$(OBJ_v) 	: obj/%.o : src/%.c src/%.h
		$(CC) -c $(FLAGS) $< -o $@ 

$(OBJ)		: obj/%.o : src/%.c
		$(CC) -c $(FLAGS) $< -o $@

value 		: $(OBJ_v)
		$(CC) $(FLAGS) -o value $(OBJ_v)  $(LINK)

value1 		: $(OBJ_v)
		$(CC) $(FLAGS) -o value1 $(OBJ_v)  $(LINK)

gconf 		: obj/gconf.o obj/energy.o obj/conf.o 
		$(CC) $(FLAGS) -o $@ obj/gconf.o obj/energy.o obj/conf.o $(LINK)

shells 		: obj/shells.o
		$(CC) $(FLAGS) -o shells obj/shells.o $(LINK)

conf_energy	: obj/conf_energy.o obj/energy.o obj/conf.o 
		$(CC) $(FLAGS) -o $@ obj/conf_energy.o obj/energy.o obj/conf.o $(LINK)


clean :
	rm  -f $(OBJ) $(OBJ_v) value1 gconf shells

