#ifndef ORIENT
#define ORIENT

#include "value.h"

int     orient_init 		(mSystem *, sParams *);
void    orient_after_step 	(mSystem *, sParams *);
void    orient_after_start 	(mSystem *, sParams *);
void    orient_final_calc 	(mSystem *, sParams *);
void    orient_print		(void *);

#endif
