#ifndef CONF
#define CONF

#include <gsl/gsl_vector.h>
#include <string.h>
#include "energy.h"

#define DELTA_X 1

static inline double pos_rest (double x, double div)
{
	if ( (x = fmod(x, div)) < 0 )
		return (x+div);
	return x;
} 
enum method { md, mc };
typedef enum method Method; 		// simulation methods 


static inline Method method_choice(char str[])
{
	if 	( strcmp (str, "md") == 0 )
		return md;
	else if ( strcmp (str, "mc") == 0 )
		return mc;

	return -1;
} 					// simulation methods choice


//----------
//General function
//----------

void    	new_conf                ( gsl_vector *R, gsl_vector *R_old, gsl_rng *rand, double max_dev);
					// new configuration R in max_dev neighborhood of configuration R_old
void    	velocity_correction     ( gsl_vector *R, double *p, Method method, double fid);
					// correction of the total momentum and angular momentum up to zero
void 		print_conf 		( gsl_vector  *R );
					// print of configuration
double 		min_distance 		(gsl_vector *R, eParams *epar);
					// minimum distance between the particles

//----------
//Functions to clusters
//----------

int 		scan_conf_cl 		( gsl_vector **R, int *flag, int *num_shells, int **sh_size, eParams *epar);
					// Scan of ground state and parameters of cluster
void    	sh_distribution 	( gsl_vector *R, double *R_sort, int *order, int *sh_size, int *shells, int **part_in_sh);


//----------
//Functions to crystal
//----------

int 		scan_conf_cr 		( gsl_vector **R_gs, int *num_x, int *num_y, eParams *epar);
					// Scan of ground state and parameters of crystal
void 		re_allocation		( gsl_vector *R, double size_x, double size_y);
					// Return the particles into the cell


#endif
