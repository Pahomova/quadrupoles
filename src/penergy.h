#ifndef POT_ENERGY
#define POT_ENERGY

#include "value.h"

int     penergy_init 		(mSystem *, sParams *);
void    penergy_after_step 	(mSystem *, sParams *);
void    penergy_after_start 	(mSystem *, sParams *);
void    penergy_final_calc 	(mSystem *, sParams *);
void    penergy_print		(void *);

#endif
