#include "conf.h"

inline int cell_number_in_distance(int i, int j){ return (i*(i-1)/2+j) ; };
void 	sort_by_shells		( gsl_vector *R, double *R_sort, int *order);

//----------
//General function
//----------

void new_conf(gsl_vector *R, gsl_vector *R_old, gsl_rng *rand, double max_dev)
{
        int 	i;
        for( i = 0; i < R->size; i++)
                gsl_vector_set(R, i  ,  gsl_vector_get(R_old, i  ) + randpoint(max_dev, rand));
}


void velocity_correction (gsl_vector *R, double *P, Method method, double fid)
{
        int 	i, num_part = R->size/2;
        double 	Px, Py, L, J = 0;

        for( i = 0 ; i < R->size ; i++)
                J += gsl_pow_2(gsl_vector_get(R, i));

        do
        {
                Px = 0;
                Py = 0;


                for( i = 0 ; i < num_part; i++)
                {
                        Px   += P[2*i  ];
                        Py   += P[2*i+1];
                }

                Px/=R->size/2;
                Py/=R->size/2;

                for( i = 0; i < num_part; i++)
                {
                        P[2*i  ] -= Px;
                        P[2*i+1] -= Py;
                }

                L  = 0;

                for( i = 0 ; i < num_part; i++)
                        L    += P[2*i]*gsl_vector_get(R, 2*i+1)-P[2*i+1]*gsl_vector_get(R, 2*i);

                L/=J;

                for( i = 0; i < num_part; i++)
                {
                        P[2*i  ] -= L*gsl_vector_get(R, 2*i+1);
                        P[2*i+1] += L*gsl_vector_get(R, 2*i  );
                }
        }
        while ( fabs(Px) > fid || fabs(Py) > fid || fabs(L) > fid );

}


void print_conf (gsl_vector *R)
{
        int i;
        for ( i = 0 ; i < R->size/2; i++)
                printf("%lf %lf \n", gsl_vector_get(R, 2*i), gsl_vector_get(R, 2*i+1));
}

double min_distance (gsl_vector *R, eParams *epar)
{
        int 	i, j;
        double  dr, x, y, dx, dy, min = -1;

        for( i = 0; i < R->size/2; i++ )
	{
             	x = gsl_vector_get(R, 2*i);
		y = gsl_vector_get(R, 2*i+1);

                for ( j = 0; j < i; j++)
		{
			dx = epar->dis(gsl_vector_get(R, 2*j  )-x, epar->size_x, epar->size_x2);
			dy = epar->dis(gsl_vector_get(R, 2*j+1)-y, epar->size_y, epar->size_y2);
			dr = gsl_hypot(dx, dy);
                        
			if ( dr < min || min < 0)
                                min = dr;
                }
	}

        return min;
}

//----------
//Functions to clusters
//----------

int scan_conf_cl ( gsl_vector **R_gs, int *flag, int *num_shells, int **sh_size, eParams *epar)
{
        int 	i, type_part, num_part;
        double 	x, y;

        if ( ! scanf("%d %d \n %d \n", &type_part, &num_part, num_shells ) )
	{
		printf("Scan_conf_cl: \t Cannot scan type particles, number of particles or number of shells. \n");
		return 1;
	}

	if (eParams_init ( epar, type_part, cl, 0, 0, 0 ))
		return 1;

        *R_gs 	 = gsl_vector_alloc (2*num_part);
        *sh_size = (int *)calloc(sizeof(int), *num_shells);

        for ( i = 0; i < *num_shells; i++)
                if ( ! scanf("%d, ", *sh_size+i) )
		{
			printf("Scan_conf_cl: \t Cannot scan shells size. \n");
			return 1;
		}

        if ( (*sh_size)[0] == 1 )
        	*flag = 1;
	else
		*flag = 0;

        for( i = 0; i < num_part; i++ )
        {
                if ( ! scanf("%lf %lf ", &x, &y))
		{
			printf("Scan_conf_cl: \t Cannot scan coodinates of %d particle. \n", i);
			return 1;
		}
                gsl_vector_set(*R_gs, 2*i  , x);
                gsl_vector_set(*R_gs, 2*i+1, y);

        }



	return 0;
}

void 	sh_distribution	(gsl_vector *R, double *R_sort, int *order, int *sh_size, int *shells, int **part_in_sh)
{
	int i, j, k, l = 0;

	k = sh_size[0];
	sort_by_shells(R, R_sort, order);

	for( i = 0, j = 0; i < R->size/2; i++ )
	{
	        if ( i == k )
		{
			l = 0;
			j ++;
	                k += sh_size[j];
		}
		part_in_sh[j][l] 	= i;
		shells[order[i]] 	= j;

		l++;
	}
}

void sort_by_shells (gsl_vector *R, double *R_sort, int *order)
{
	int 	i, j, buf, num_part;

	num_part = R->size/2;

	for( i = 0; i < num_part; i++ )
	{
		order[i]  = i;
		R_sort[i] = gsl_hypot(gsl_vector_get(R, 2*i), gsl_vector_get(R, 2*i+1));
	}

	for( i = 0; i < num_part; i++ )
		for( j = 1; j < num_part-i; j++)
			if ( R_sort[order[j-1]] > R_sort[order[j]] )
			{
				buf 	 	= order[j-1];
				order[j-1] 	= order[j];
				order[j]   	= buf;
			}

}

//----------
//Functions to crystals
//----------

int 	scan_conf_cr ( gsl_vector **R_gs, int *num_x, int *num_y, eParams *epar)
{
	int 	i, j, k, type_part;
   	double	x, y, dx, dy, dx2, dy2, Rc;

	if ( scanf("%d %d %d %lf\n", &type_part, num_x, num_y, &Rc) == 0)
	{
        	printf("Scan_conf_cr: \t cannot scan type particles, number particles in lattice or cutoff radius. \n");
		return 1;
	}

	dx 	 = DELTA_X;
	dy 	 = dx*sqrt(3);

	dx2 	= dx/2;
	dy2 	= dy/2;

        if ( eParams_init ( epar, type_part, cr, *num_x*dx, *num_y*dy, Rc*dx) )
		return 1;

        *R_gs	= gsl_vector_alloc (4**num_x**num_y);

	for ( k = 0, i = 0; i < *num_x; i++)
	{
		x = i*dx - epar->size_x2;

		for ( j = 0; j < *num_y; j++ )
		{	
			y = j*dy - epar->size_y2;

			gsl_vector_set(*R_gs, 2*k,   x);
			gsl_vector_set(*R_gs, 2*k+1, y);
			k++;

			gsl_vector_set(*R_gs, 2*k,   x+dx2);
			gsl_vector_set(*R_gs, 2*k+1, y+dy2);
			k++;
		}
	}


	return 0;
}

void re_allocation( gsl_vector *R, double size_x, double size_y)
{
	int 	i;

	for ( i = 0; i < R->size/2; i++)
	{
		gsl_vector_set(R, 2*i  , remainder(gsl_vector_get(R, 2*i  ), size_x));
		gsl_vector_set(R, 2*i+1, remainder(gsl_vector_get(R, 2*i+1), size_y));
	}


}















