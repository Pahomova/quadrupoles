#ifndef METHOD_MC
#define METHOD_MC

#include "energy.h"
#include "conf.h"
#include "value.h"

int 	dynamic_init_mc		( mSystem *msys, sParams *spar);
int 	dynamic_mc	 	( mSystem *msys, sParams *spar);

#endif
