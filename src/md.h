#ifndef METHOD_MD
#define METHOD_MD

#include "energy.h"
#include "conf.h"
#include "value.h"

int 	dynamic_init_md 	( mSystem *msys, sParams *spar);
int 	dynamic_md 	  	( mSystem *msys, sParams *spar);

#endif
