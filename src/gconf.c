#include <time.h>
#include <gsl/gsl_multimin.h>

#include "energy.h"

void 		print_conf		(gsl_vector *R);
void 		rand_search 		(gsl_vector *x, gsl_vector *y, gsl_rng *rand, int num_rand_search, double size, eParams *epar);
void 		grad_des 		(gsl_vector *R, int num_grad_steps, double step, double tol, double epsabs, eParams *epar);



int main (int argc, char *argv[])
{

	int 	i, type_part, num_part, time_flag = 0,

		starts		= 1e+2,
		num_grad_steps	= 5e+3,
		num_rand_search	= 1e+3;

	double
		grad_des_step	= 1e-4,
		tol		= 1e-5,
		size		= 10,
		epsabs		= 1e-8;

	eParams epar;
	time_t 	time_0;

	gsl_rng 	*rand;
	gsl_vector 	*R, *x, *y;

	time_0 = time(NULL);

	if ( argc < 3 || argc > 4 )
	{
		printf("Use:  {type_part} {num_part} [time_flag]\n");
		return 1;
	}

	type_part	= atoi(argv[1]);
	num_part 	= atoi(argv[2]);

	if ( argc == 4 && strcmp(argv[3], "-t") == 0 )
		time_flag = 1;

	eParams_init(&epar, type_part, cl, 0, 0, 0);

	rand		= gsl_rng_alloc(gsl_rng_mt19937);
	R  		= gsl_vector_alloc (2*num_part);
	x 		= gsl_vector_alloc (2*num_part);
	y 		= gsl_vector_alloc (2*num_part);

	gsl_vector_rng(R, rand, 2*num_part);
	grad_des(R, num_grad_steps, grad_des_step, tol, epsabs, &epar);

	for ( i = 0; i < starts; i++)
	{
		rand_search(x, y, rand, num_rand_search, size, &epar);
		grad_des(x, num_grad_steps, grad_des_step, tol, epsabs, &epar);

		if ( pot_energy(R, &epar) > pot_energy(x, &epar) )
			swap(&R, &x);
	}

	if (time_flag)
		printf("%.0lf\n", difftime(time(NULL), time_0));

	print_conf(R);

	return 0;
}


void rand_search (gsl_vector *x, gsl_vector *y, gsl_rng *rand, int num_rand_search, double size, eParams *epar)
{
	int i;
	double H, h;

	gsl_vector_rng(x, rand, size);
	H=pot_energy(x, epar);

	for ( i = 0; i < num_rand_search; i++ )
	{
		gsl_vector_rng(y, rand, size);
		h=pot_energy(y, epar);

		if ( H > h )
		{
			swap(x, y);
			H=h;
		}
	}
}

void grad_des (gsl_vector *R, int num_grad_steps, double step, double tol, double epsabs, eParams *epar)
{
	int i = 0, status;
	gsl_multimin_fdfminimizer *r;
	gsl_multimin_function_fdf func;

	func.n 		= R->size;
	func.f 		= pot_energy;
	func.df 	= grad_of_pot_energy;
	func.fdf 	= pot_energy_grad;
	func.params 	= epar;

	r = gsl_multimin_fdfminimizer_alloc (gsl_multimin_fdfminimizer_conjugate_fr, R->size);

	gsl_multimin_fdfminimizer_set (r, &func, R, step, tol);

	do
	{
		i++;
		status = gsl_multimin_fdfminimizer_iterate (r);

		if (status)
			break;

		status = gsl_multimin_test_gradient (r->gradient, epsabs);
	}
	while ( status == GSL_CONTINUE && i < num_grad_steps );

	gsl_vector_memcpy(R, r->x);
	gsl_multimin_fdfminimizer_free (r);
}

