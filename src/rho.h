#ifndef RHO_H
#define RHO_H

#include "value.h"

int     rho_init 	(mSystem *, sParams *);
void    rho_after_step 	(mSystem *, sParams *);
void    rho_after_start (mSystem *, sParams *);
void    rho_final_calc 	(mSystem *, sParams *);
void    rho_print	(void *);

#endif
