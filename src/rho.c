#include "rho.h"

#define NUM_INTERVALS 200

inline int coord ( double r, double zone, int num_int) { return floor(r/zone*num_int); }

struct value_rho
{
	int 	num_int; 	// number of partition zone intervals
	double 	*rho,		// rho
		zone;		// considered zone 
};
typedef struct value_rho Rho;

int     rho_init  (mSystem *msys, sParams * spar)
{
	Rho	*rho= (Rho *) calloc (sizeof(Rho), 1);

	rho->zone	= fmin(spar->epar->size_x, spar->epar->size_y) / 2;
	rho->num_int	= NUM_INTERVALS;
	rho->rho	= (double*) calloc (sizeof(double), rho->num_int);
	
	spar->value		= rho;
	spar->calc_after_step   = rho_after_step;
	spar->calc_after_start  = rho_after_start;
	spar->final_calc        = rho_final_calc;
	spar->print_result      = rho_print;

	spar->starts		= 1;

	return 0;
}

void    rho_after_step (mSystem * msys,  sParams *spar )
{
	int 	i, j, n;
	double	x, y, dx, dy;

	Rho	*rho = (Rho *) spar->value;
//	print_conf(msys->R);
// return;
	for ( i = 0; i < msys->num_part; i++ )
	{
		x = gsl_vector_get(msys->R, 2*i);
		y = gsl_vector_get(msys->R, 2*i+1);

		for ( j = 0; j < i; j++ )
		{
			dx = spar->epar->dis(gsl_vector_get(msys->R, 2*j  )-x, spar->epar->size_x, spar->epar->size_x2);
//printf("A %lf %lf || %lf %lf\n", x, gsl_vector_get(msys->R, 2*j), dx, spar->epar->size_x);
			dy = spar->epar->dis(gsl_vector_get(msys->R, 2*j+1)-y, spar->epar->size_y, spar->epar->size_y2);

			n  = coord(gsl_hypot(dx, dy), rho->zone, rho->num_int);

			if ( n < rho->num_int && n > 0 )
				rho->rho[n] ++;
		}
	}
}

void    rho_after_start (mSystem * msys,  sParams *spar ) 	{}
	
void    rho_final_calc (mSystem * msys,  sParams *spar )
{
	int 	i;
	double  k, density;
	Rho	*rho = (Rho *) spar->value;

	density	= msys->num_part/spar->epar->size_x/spar->epar->size_y;
	k 	= spar->steps * density * msys->num_part * M_PI * gsl_pow_2(rho->zone/rho->num_int);

	for ( i = 1 ; i < rho->num_int ; i++)
		rho->rho[i] /= k*i;
}

void    rho_print (void *value)				
{
	int 	i;
	double 	dx;
	Rho	*rho = (Rho *) value;
	
	dx = rho->zone/rho->num_int;

	for ( i = 0; i < rho->num_int; i++ )
		printf("%lf %lf\n", i*dx+dx/2, rho->rho[i]);
}



