#include "trajs.h"

double  T;

int     trajs_init  (mSystem * msys, sParams *spar)
{
	T = 0;

	spar->starts 		= 1;
	spar->steps 		= 100;

        spar->calc_after_step   = trajs_after_step;
        spar->calc_after_start  = trajs_after_start;
        spar->final_calc        = trajs_final_calc;
        spar->print_result      = trajs_print;

	return 0;
}

void    trajs_after_step (mSystem * msys, sParams *spar)
{
printf("!!!\n");
	T += temperatura(msys->p, msys->num_part);
printf("!!! T = %lf num_part=%d \n", T, msys->num_part);
	//if ( spar->meas % 1 == 0)
	//	print_conf(msys->R_gs);
}

void    trajs_after_start (mSystem * msys, sParams *spar)
{
}

void    trajs_final_calc (mSystem * msys,  sParams *spar)
{
	T /= spar->meas;
}

void    trajs_print (void *value)
{
	printf("%lf\n", T);

}

