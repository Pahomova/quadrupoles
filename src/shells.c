#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_math.h>

int 	shells 	( double *R, int *shell, int num_part, int s);

int main (int argc, char *argv[])
{

	int             i, j, num_part, N, n, *shell;

	double          *R, min, d;


	if ( argc != 2 )
	{
	        printf("Use: %s {num_part} \n", argv[0]);
	        return 1;
	}


	num_part  = atoi(argv[1]);

	R 	= (double *) calloc(sizeof(double), 2*num_part);
	shell	= (int *) calloc(sizeof(int), num_part);

	for( i = 0; i < num_part; i++ )
		if ( scanf("%lf %lf\n", R+2*i, R+2*i+1) != 2 )
		{
			printf("Error in scan configurations\n");
			return 1;
		}

	min = gsl_hypot(R[0], R[1]);

	for( i = 1; i < num_part; i++ )
	{
		d=gsl_hypot(R[2*i], R[2*i+1]);
		if ( min > d )
			min  = d;
	}

	for( i = 0; i < num_part; i++ )
		if (gsl_hypot(R[2*i], R[2*i+1]) <= 1.5*min )
			shell[i] = 1;

	N = shells(R, shell, num_part, 2);

	printf("%d\n", N);

	for ( i = 1; i <= N ; i++)
		{
			for ( j = 0, n = 0; j < num_part; j++ )
				if ( shell[j] == i )
					n++;
			printf("%d", n);
			if ( i != N )
				printf(", ");
		}

	printf("\n");

	for ( i = 1; i <= N ; i++)
		for ( j = 0; j < num_part; j++ )
			if ( shell[j] == i )
				printf("%lf %lf \n", R[2*j], R[2*j+1]);

	return 0;
}


int shells (double *R,  int *shell, int num_part, int s)
{
	int 	i, j, flag = 0 ;
	double 	d, min = 0 ;

	for ( i = 0; i < num_part; i++)
		if ( shell[i] == s-1 )
			for ( j = 0; j < num_part; j++)
			{
				if ( shell[j] != 0 )
					continue;
				d=gsl_hypot(R[2*i]-R[2*j], R[2*i+1]-R[2*j+1]);

				if ( flag == 0 )
				{
					min  = d;
					flag = 1;
				}
				else if ( min > d )
					min  = d;
			}

	if ( min == 0 )
		return s-1;

	for ( i = 0; i < num_part; i++)
		if  ( shell [i] == s-1)
			for ( j = 0; j < num_part; j++)
				if ( shell[j] == 0 && gsl_hypot(R[2*i]-R[2*j], R[2*i+1]-R[2*j+1])/min < 1.5)
					shell[j] = s;


	return shells(R, shell, num_part, s+1);

}
