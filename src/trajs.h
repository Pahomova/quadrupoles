#ifndef TRAJS
#define TRAJS

#include "value.h"

int     trajs_init 		(mSystem *, sParams *);
void    trajs_after_step 	(mSystem *, sParams *);
void    trajs_after_start 	(mSystem *, sParams *);
void    trajs_final_calc 	(mSystem *, sParams *);
void    trajs_print		(void *);

#endif
