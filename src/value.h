#ifndef VALUES
#define VALUES

#include <time.h>
#include <math.h>

#include "conf.h"
#include "energy.h"

#define N_PARAM			1	// constant in step_choice
#define STEP_0 			0.01	// max step_size

#define ACCURACY_T 		1e-3	// accuracy for temperatura
#define ACCURACY_p 		1e-5	// accuracy for momenta

#define MAX_DEV			1	// the largest deviation coordinate from the last position
#define MAX_VEL			0.1	// the largest projection of velocity on axle

#define PER_SHIFT		10	// percent of shifted particles before one measurement
#define PERIOD 			4	// number of single-particle shifts to a general shift divided by the number of particles

//----------
// Number of steps and starts
//----------
// TRIAL_STEPS  -- number of trial steps to initalization simulating
// STEPS 	-- number of steps on one start of simulating
// STARTS	-- number of simulation starts

//Cluster
//MD 
#define TRIAL_STEPS_MD_CL 	1e+5	
#define STEPS_MD_CL		5e+6	
#define STARTS_MD_CL		10	


//MC 
#define TRIAL_STEPS_MC_CL 	1e+5	 
#define STEPS_MC_CL		1e+6	
#define STARTS_MC_CL		10	

//Crystal
//MD
#define TRIAL_STEPS_MD_CR 	1e+4	
#define STEPS_MD_CR		1e+4	
#define STARTS_MD_CR		10	

//MC
#define TRIAL_STEPS_MC_CR 	1e+4	
#define STEPS_MC_CR		1e+4	
#define STARTS_MC_CR		10	

//----------
// Сalculated value
//----------
enum value { temp, fenergy, penergy, delta, orient, trajs, rho };
typedef enum value Value;

static inline Value value_flag_choice(char str[])
{
	if ( strcmp (str, "fenergy") == 0 )
		return fenergy;
	if ( strcmp (str, "penergy") == 0 )
		return penergy;
	if ( strcmp (str, "delta") == 0 )
		return delta;
	if ( strcmp (str, "orient") == 0 )
		return orient;
	if ( strcmp (str, "trajs") == 0 )
		return trajs;
	if ( strcmp(str, "temp") == 0)
		return temp;
	if ( strcmp(str, "rho") == 0)
		return rho;

	printf("value_flag_choice: \t unknown value_flag.\n");
	return -1;
}

//----------
//Parameters of simulated system
//----------
struct  mod_system
{
	int		now_part, 	// current number of shifted particles
			num_part,	// number of particles in the system

	 		num_shells, 	// number of shells

			*order,		// order of particles
			*sh_size,	// amount of particles in each shell
			*sh_of_part,	// number of shell for each particle
			**part_in_sh,	// part_in_sh[i][j] number of j-particle in the i-shell

			s[2],		// considered shells
			flag,		// flag: first shell consists of one particle (1) or not (0)

			nx,		// number of particles in lattice on the wight
			ny;		// number of particles in lattice on y direction

	double 		*sort,		// array to sort particle by radius vector
			*p, 		// particle momenta 
                	H;              // current energy

	gsl_vector 	*R, 		// current coordinates of particles
                	*r, 		// possible coordinates of particles
                	*R_gs, 		// ground state of the system
                	*f;		// forces
};
typedef struct mod_system mSystem;


//----------
//Parameters of simulating
//----------
typedef struct simulation_params sParams;
struct simulation_params
{
	int	trial_steps, 		// number of trial steps
		starts,			// number of starts simulating

		now_shift,		// current single-particle shift in MC
		shift_part_meas,	// account of shifted particles befor measement
		period_mc, 		// number of steps, after which the whole system shifts

		acc,			// flag: last configuration was accepted (1) or not (0)
		time_flag;		// flag: program prints the time of work (1) or not (0)

	long 	steps,			// number of simulation steps
       		meas;           	// number of measuments in MC

	Value 	value_flag;		// calculated value
	void*	value;			// structures and functions for calculated value

        double  T,              	// temperature of simulating
                step,           	// time step size

                max_dev,        	// maximum projection on the axis of the initial deviation from the lattice point
                max_vel,        	// maximum projection on the axis of the initial particle velocity

		fid_T,      		// accuracy of temperatura
                fid_p;      		// accuracy of zero total momentum and angular momentum of system

        gsl_rng *rand;          	// random number generator
        eParams	*epar;			// parameters of energy
	Method  method_flag;    	// simulating method: molecular dynamics (md) or monte-carlo (mc)

	//Dynamic functions :

        int 	(*dynamic_init)		( mSystem *, sParams *);
        int    	(*dynamic)      	( mSystem *, sParams *);

	//Calculation functions :

	void 	(*calc_after_step)	(mSystem *, sParams *);
	void 	(*calc_after_start)	(mSystem *, sParams *);
	void 	(*final_calc)		(mSystem *, sParams *);
	void    (*print_result) 	(void *);
};



#endif
