#ifndef FULL_ENERGY
#define FULL_ENERGY

#include "value.h"

int     fenergy_init 		(mSystem *, sParams *);
void    fenergy_after_step 	(mSystem *, sParams *);
void    fenergy_after_start 	(mSystem *, sParams *);
void    fenergy_final_calc 	(mSystem *, sParams *);
void    fenergy_print		(void *);

#endif
