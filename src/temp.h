#ifndef TEMPERATURA
#define TEMPERATURA

#include "value.h"

int     temp_init 		(mSystem *, sParams *);
void    temp_after_step 	(mSystem *, sParams *);
void    temp_after_start 	(mSystem *, sParams *);
void    temp_final_calc 	(mSystem *, sParams *);
void    temp_print		(void *);

#endif
