#include "delta.h"

void zero(double *x,  int n)
{
	for( n--; n >= 0; n--)
		x[n] = 0;
}

struct value_delta
{
	int 	start, step;
	double 	T, *buf, *a, *a2,  *delta, adelta, sdelta;
};
typedef struct value_delta Delta;

int     delta_init  (mSystem *msys, sParams * spar)
{
	Delta *delta = (Delta *) calloc (sizeof(Delta), 1);

	if ( spar->epar->sys_type == cr )
	{
		printf("Delta isn't defined for crystal.\n");
		return 1;
	}

	delta->buf 	= (double *) calloc (sizeof(double), msys->num_part);
	delta->a   	= (double *) calloc (sizeof(double), msys->num_part);
	delta->a2  	= (double *) calloc (sizeof(double), msys->num_part);
        delta->delta	= (double *) calloc (sizeof(double), spar->starts  );

	delta->T	= 0;
	delta->adelta 	= 0;
	delta->sdelta 	= 0;
	delta->start 	= 0;
	delta->step 	= 0;

	spar->value             = delta;
	spar->calc_after_step   = delta_after_step;
	spar->calc_after_start  = delta_after_start;
	spar->final_calc        = delta_final_calc;
	spar->print_result      = delta_print;

	return 0;
}

void    delta_after_step (mSystem * msys, sParams *spar)
{
	int 	i;
	Delta 	*delta = (Delta *) spar->value;

	if ( spar->acc )
		for( i = msys->flag; i < msys->num_part; i++ )
			delta->buf[i] = gsl_hypot(gsl_vector_get(msys->R, 2*i), gsl_vector_get(msys->R, 2*i+1));

	delta->step++;

	for( i = msys->flag; i < msys->num_part; i++ )
	{
		delta->a [i]	+= delta->buf[i];
		delta->a2[i]	+= gsl_pow_2(delta->buf[i]);
	}


}

void    delta_after_start (mSystem * msys, sParams *spar)
{
	int 	i;
	Delta 	*delta = (Delta *) spar->value;
	delta->step = 0;

	for( i = msys->flag; i < msys->num_part; i++ )
	{
		delta->a [i]	/= spar->meas;
		delta->a2[i]	/= spar->meas;
	}

	for( i = msys->flag; i < msys->num_part; i++ )
		delta->delta[delta->start]  	+= sqrt( fabs(delta->a2[i]/gsl_pow_2(delta->a[i])-1) );

	delta->delta[delta->start]	/= (msys->num_part) - (msys->flag);
	delta->adelta 			+= delta->delta[delta->start];
	delta->start++;

	zero(delta->a , msys->num_part);
	zero(delta->a2, msys->num_part);

}

void    delta_final_calc (mSystem * msys, sParams *spar)
{
	int 	i;
	Delta 	*delta = (Delta *) spar->value;

	delta->adelta /= spar->starts;

	for ( i = 0; i < spar->starts; i++ )
		delta->sdelta += gsl_pow_2(delta->delta[i]-delta->adelta);

	delta->sdelta 	= sqrt(delta->sdelta/(spar->starts-1));
	delta->T  	= spar->T;

}

void    delta_print (void *value)
{
	Delta 	*delta = (Delta *) value;
	printf("%lf \t %lf \t %lf\n", delta->T, delta->adelta, delta->sdelta);
}

