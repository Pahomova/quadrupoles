#include "md.h"

void 	new_conf_md	(gsl_vector *R, gsl_vector *R_old, double *p, sParams *spar);
			// create new configuration
double 	step_choice 	(gsl_vector *R, double N, double T, double step_0, eParams *epar);
			// choice of time step to dynamic

int dynamic_init_md ( mSystem *msys,  sParams *spar)
{
        int     i;
        double  T = spar->T+1, k;

	spar->max_dev 	= 0;
	spar->step	= step_choice	     (msys->R_gs, N_PARAM, spar->T, STEP_0, spar->epar);

        do
	{
		if ( T - spar->T > 0 ) 
		{
			new_conf_md(msys->R, msys->R_gs, msys->p, spar);

			for (i = 0; i < spar->trial_steps ; i++)
				dynamic_md(msys, spar);
		}

		for ( T = 0, i = 0; i < spar->trial_steps ; i++ )
                {
                        dynamic_md(msys, spar);
                        T += temperatura(msys->p, msys->num_part);

                }

		T /= spar->trial_steps;
		k  = sqrt (spar->T/T);
                for ( i = 0; i < msys->R->size ; i++)
                        msys->p[i] *= k;
        }
	while ( fabs(spar->T-T) > spar->fid_T );
	return 0;
}


//----------
//Dynamic  functions
//----------

int dynamic_md (mSystem *msys, sParams *spar)
{
        int i;

        grad_of_pot_energy(msys->R, (void *)spar->epar, msys->f);

        for( i = 0; i < msys->R->size; i++ )
        {
                msys->p[i] -= gsl_vector_get(msys->f, i)*spar->step;
                gsl_vector_element_add(msys->R, i, msys->p[i]*spar->step);
        }

	if ( spar->epar->sys_type == cr)
		re_allocation(msys->R, spar->epar->size_x, spar->epar->size_y);

	return 1;
}


//----------
//Auxiliary functions
//----------

void new_conf_md(gsl_vector *R, gsl_vector *R_old, double *p, sParams *spar)
{
        int 	i;
        double 	k;

        for( i = 0 ; i < R->size; i++)
		p[i]	= randpoint(spar->max_vel, spar->rand);

	new_conf 		(R, R_old, spar->rand, spar->max_dev);
        velocity_correction 	(R, p, spar->method_flag, spar->fid_p);

	k = sqrt(1.75*spar->T/temperatura(p, R->size/2));
	for( i = 0 ; i < R->size ; i++)
		p[i] *= k;
}

double step_choice (gsl_vector *R, double N, double T, double step_0, eParams *epar)
{
        double step;

        step = min_distance(R, epar)/N/sqrt(T);
	
        if ( step >= step_0 )
                return step_0;

        return step;
}

