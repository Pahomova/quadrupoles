#include "energy.h"
#include "conf.h"

//Auxiliary functions

inline double	eqv 		(double x)  				{ return x; };
inline double	eqv2		(double x, double size, double size2) 	{ return x; };
double	(*select_pow(int N))(double );	

// External potential

inline double harmonic_pot	(double x, double y) 		{ return gsl_pow_2(x)+gsl_pow_2(y); };
inline double non_pot		(double x, double y) 		{ return 0; };



double cluster_dis(double x, double size, double size2 )
{
	if ( x > size2 )
		return x-size;
	else if ( x < -size2)
		return x+size;
	return x;
}
//----------
//Functions to potential energy
//----------

double 	pot_energy (const gsl_vector * R, void * Epar)
{
	int     i, j;
	double  x, y, dx, dy, dr, U = 0;
	eParams *epar = (eParams *) Epar;

	for ( i = 0; i < R->size/2; i++ )
	{
		x = gsl_vector_get(R, 2*i);
		y = gsl_vector_get(R, 2*i+1);

                for ( j = 0; j < i; j++)
		{
			dx = epar->dis(gsl_vector_get(R, 2*j  )-x, epar->size_x, epar->size_x2);
			dy = epar->dis(gsl_vector_get(R, 2*j+1)-y, epar->size_y, epar->size_y2);
			dr = gsl_hypot(dx, dy);
		
			if ( dr < epar->Rc )
				U  += 1/epar->pow(dr);
                }

		U += epar->ex_pot(x, y);
	}
	return U;
}

double pot_energy_1p(const gsl_vector * R, void * Epar, int N)
{
	int     j;
	double  x, y, dx, dy, dr, U = 0;
	eParams *epar = (eParams *) Epar;

	x = gsl_vector_get(R, 2*N);
	y = gsl_vector_get(R, 2*N+1);

        for ( j = 0; j < R->size/2; j++)
	{
		if ( N == j )
			continue;

		dx = epar->dis(gsl_vector_get(R, 2*j  )-x, epar->size_x, epar->size_x2);
		dy = epar->dis(gsl_vector_get(R, 2*j+1)-y, epar->size_y, epar->size_y2);
		dr = gsl_hypot(dx, dy);
		
		if ( dr < epar->Rc )
			U  += 1/epar->pow(dr);
	}

	U += epar->ex_pot(x, y);
	return U;
}

void grad_of_pot_energy (const gsl_vector * R, void * Epar, gsl_vector * grad)
{
        int 	i, j;
        double 	x, y, dx, dy, dr, der, a;
	eParams *epar = (eParams *) Epar;

	gsl_vector_set_zero(grad);
	for ( i = 0; i < R->size/2; i++ )
	{
		x = gsl_vector_get(R, 2*i);
		y = gsl_vector_get(R, 2*i+1);

		for ( j = 0; j < i; j++ )
		{
			dx = epar->dis(gsl_vector_get(R, 2*j  )-x, epar->size_x, epar->size_x2);
			dy = epar->dis(gsl_vector_get(R, 2*j+1)-y, epar->size_y, epar->size_y2);
			dr = gsl_hypot(dx, dy);

			if ( dr < epar->Rc )
			{

				a  = epar->type_part/epar->pow2(dr);

				der = a*dx;
				gsl_vector_element_add(grad, 2*i, der);
				gsl_vector_element_add(grad, 2*j, (-1)*der);

				der = a*dy;
				gsl_vector_element_add(grad, 2*i+1, der);
				gsl_vector_element_add(grad, 2*j+1, (-1)*der);
			}
		}

		if ( epar->sys_type == cl )
		{
			gsl_vector_element_add(grad, 2*i  , 2*x);
			gsl_vector_element_add(grad, 2*i+1, 2*y);
		}
	}
}

void pot_energy_grad (const gsl_vector *R, void * epar, double *U, gsl_vector *grad)
{
	*U = pot_energy(R, epar);
	grad_of_pot_energy(R, epar, grad);
}


//----------
//Functions to kinetic energy
//----------

double kin_energy(double *p, int num_part)
{
	int 	i;
	double 	K = 0;

	for( i = 0; i < 2*num_part; i++ )
		K += gsl_pow_2( p[i] );

	return K/2;
}

double temperatura(double *p, int num_part)
{
	return kin_energy(p, num_part)/num_part;
}

//----------
//Auxiliary functions
//----------

int eParams_init ( eParams *epar, int type_part, int sys_type, double size_x, double size_y, double Rc)
{
	epar->type_part 	= type_part;
	epar->sys_type 		= sys_type;
        epar->pow  		= select_pow(type_part  );
        epar->pow2 		= select_pow(type_part+2);

	switch ( epar->sys_type )
	{
		case cl:
			epar->ex_pot	= harmonic_pot;
			epar->dis	= eqv2;

			epar->size_x	= 0;
			epar->size_y	= 0;

			epar->size_x2	= 0;
			epar->size_y2	= 0;

			epar->Rc	= fabs(1.0/0.0);

			break;

		case cr:

			epar->ex_pot	= non_pot;
			epar->dis	= cluster_dis;

			epar->size_x	= size_x;
			epar->size_y	= size_y;

			epar->size_x2	= size_x/2;
			epar->size_y2	= size_y/2;

			epar->Rc	= Rc;

			break;

		default:
			printf("eParams_init: \t Unknown system type\n");
			return 1;
	}

	return 0;
}



double		(*select_pow(int N))(double )
{
	switch(N)
	{
		case 1:
			return eqv;
		case 2:
			return gsl_pow_2;
		case 3:
			return gsl_pow_3;
		case 4:
			return gsl_pow_4;
		case 5:
			return gsl_pow_5;
		case 6:
			return gsl_pow_6;
		case 7:
			return gsl_pow_7;
		case 8:
			return gsl_pow_8;
		case 9:
			return gsl_pow_9;
		default:
			return NULL;
	}
}

void gsl_vector_rng (gsl_vector *x, gsl_rng *rand, double size)
{
	int i;

	for ( i = 0; i < x->size; i++ )
		gsl_vector_set(x, i, randpoint(size, rand));
}
