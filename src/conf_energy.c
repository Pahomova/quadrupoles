#include <stdio.h>
#include <math.h>

#include "energy.h"

int main ()
{
	int 		i, n, a, type_part, num_part;
	double 		x, y, U;

	gsl_vector	*R;
	eParams 	epar;


	if ( ! scanf ("%d %d\n", &type_part, &num_part) || ! scanf ("%d\n", &n))
	{
		printf("Error in scan type_part, num_part or num_shells.\n");
		return 1;
	}

	R = gsl_vector_alloc(2*num_part);
	eParams_init(&epar, type_part, cl, 0, 0, 0);

	for( i = 0; i < n; i++ )
		if (! scanf("%d,", &a) )
		{
			printf("Error in scan shells.\n");
			return 1;
		}

	for( i = 0; i < num_part; i++ )
        {
                if ( ! scanf("%lf %lf ", &x, &y) )
		{
			printf("Error in scan coordinetes of %i particle\n", i);
			return 1;
		}
                gsl_vector_set(R, 2*i  , x);
                gsl_vector_set(R, 2*i+1, y);
        }

	U = pot_energy(R, &epar);

	printf("%d %lf %lf \n", num_part, U, U/num_part);

	return 0;
}
