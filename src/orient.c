#include "orient.h"

#define COUNTER 10

void Psi_filling(gsl_vector *R, double complex *Psi, int *part_in_sh, int sh_size );

struct value_orient
{
        int		start,		// number of current start
			counter;	// steps before next sorting

        double		sS, 		// standard deviation
			T;		// system temperatura 

        double complex  Psi[2],		// psi of shells
			*S,		// the angular correlation function 
			aS,		// value averaged by starts
			buf;		// buffer
};
typedef struct value_orient Orient;

int     orient_init  (mSystem *msys, sParams * spar)
{
	Orient 		*orient = (Orient *) calloc (sizeof(Orient), 1);

	if ( spar->epar->sys_type == cr )
	{
		printf("Delta isn't defined for crystal.\n");
		return 1;
	}

	if ( msys->s[0] < 0 || msys->s[1] < 0 )
	{
		printf("s1 and s2 aren't defined\n");
		return 1;
	}

	orient->S		= (double complex *) 	calloc (sizeof(double complex), spar->starts);

	orient->T 		= 0;
	orient->aS 		= 0;
	orient->sS 		= 0;
	orient->start 		= 0;
	orient->counter		= 0;

	spar->value             = orient;
	spar->calc_after_step   = orient_after_step;
	spar->calc_after_start  = orient_after_start;
	spar->final_calc        = orient_final_calc;
	spar->print_result      = orient_print;

	return 0;
}

void    orient_after_step (mSystem * msys, sParams *spar)
{
	Orient	*orient = (Orient *) spar->value;

	if ( spar->method_flag == md  || spar->acc )
	{

		if ( orient->counter == 0 )
		{
			orient->counter = COUNTER;
			sh_distribution (msys->R, msys->sort, msys->order, msys->sh_size, msys->sh_of_part, msys->part_in_sh);
		}

		Psi_filling(msys->R, orient->Psi  , msys->part_in_sh[msys->s[0]], msys->sh_size[msys->s[0]] );
		Psi_filling(msys->R, orient->Psi+1, msys->part_in_sh[msys->s[1]], msys->sh_size[msys->s[1]] );

	        orient->buf   = orient->Psi[0]*conj(orient->Psi[1]);
	        orient->buf  /= msys->sh_size[msys->s[0]];
	        orient->buf  /= msys->sh_size[msys->s[1]];
	}

	orient->S[orient->start] += orient->buf;
	orient->counter--;

}

void    orient_after_start (mSystem * msys, sParams *spar)
{
	Orient	*orient = (Orient *) spar->value;

//printf("%ld %lf %lf\n", spar->meas, cabs(orient->S[orient->start]), cabs(orient->S[orient->start]/spar->meas));

	orient->S[orient->start] /= spar->meas;

	orient->aS		 += orient->S[orient->start];
	orient->start++;

}

void    orient_final_calc (mSystem * msys, sParams *spar)
{
	int 		i;
	Orient		*orient = (Orient *) spar->value;

	orient->T   = spar->T;
	orient->aS /= spar->starts;

	for( i = 0; i < spar->starts; i++ )
		orient->sS += gsl_pow_2( orient->S[i] - orient->aS );

	orient->sS = sqrt(orient->sS / spar->starts / (spar->starts-1));
}

void    orient_print (void *value)
{
	Orient	*orient = (Orient *) value;

	printf("%lf \t %lf \t %lf\n", orient->T, cabs(orient->aS), orient->sS);

}

//----------
//Auxiliary functions
//----------

void Psi_filling(gsl_vector *R, double complex *Psi, int *part_in_sh, int sh_size )
{
	int 		i, j;
	double 		phi;
	complex double 	a;

	*Psi	= 0;
	a 	= I*sh_size;

	for( i = 0; i < sh_size ; i++ )	
	{
		j 	= part_in_sh[i]*2;
		phi	= atan2(gsl_vector_get(R, j+1), gsl_vector_get(R, j));
		*Psi   += cexp( a*phi );
	}
}
