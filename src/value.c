#include "value.h"

#include "mc.h"
#include "md.h"

#include "fenergy.h"
#include "penergy.h"
#include "delta.h"
#include "orient.h"
#include "trajs.h"
#include "temp.h"
#include "rho.h"

int 	simulation 		(mSystem *msys, sParams *spar);

//Initialization functions:
int 	scan_system_conf	(mSystem *msys, eParams *epar, int argc, char *argv[]);
int 	sParams_init 		(sParams *spar, int argc, char *argv[],  int num_part, eParams *epar);
int	mSystem_init 		(mSystem *msys, int argc, char *argv[],  sParams *spar);
int 	value_init		(mSystem *msys, int argc, char *argv[], sParams *spar);

int main(int argc, char *argv[])
{
	time_t		time_0 = time(NULL);

        eParams		epar;
        sParams		spar;
	mSystem		msys;

	if ( argc < 5 || argc > 8 || strcmp(argv[1], "help") == 0 ) 
	{
		printf("Use: %s [system_type] [method] [value] [T] {shell_1} {sheel_2} {-t} \n", argv[0]);
		return 1; 
	}

	if ( scan_system_conf (&msys, &epar, argc, argv) )
	{
		printf("Main:  error in scan_system_conf.\n");
		return 1;
	}

	if ( sParams_init (&spar, argc, argv, msys.R_gs->size/2, &epar)  )
	{
		printf("Main:  error in sParams_init.\n");
		return 1;
	}

	if ( mSystem_init (&msys, argc, argv, &spar) )
	{
		printf("Main: error in mSystem_init.\n");
		return 1;
	}

	if ( value_init (&msys,  argc, argv, &spar) )
	{
		printf("Main:  error in mSystem_init.\n");
		return 1;
	}

	if ( simulation(&msys, &spar) )
	{
		printf("Main:  error in simulation.\n");
		return 1;
	}


	if ( spar.time_flag )
		printf("%.0lf\n", difftime(time(NULL), time_0));	

	return 0;

}


int simulation (mSystem *msys, sParams *spar)
{
	int 	i, j;

        if ( spar->dynamic_init (msys, spar) )
		return 1;

	for( i = 0; i < spar->starts; i++ )
	{
		spar->acc 	= 0;
		spar->meas	= 0;
		spar->acc 	= 1;

		if ( spar->method_flag == mc )
		{
			new_conf(msys->R, msys->R_gs, spar->rand, spar->max_dev);
			msys->H 	= pot_energy(msys->R, spar->epar);
		}

		for( j = 0; j < spar->steps; j++ )
		{
			if ( spar->method_flag == md || spar->now_shift % spar->shift_part_meas == 0 )
			{
				spar->calc_after_step(msys, spar);
				spar->meas++ ;
			}
			spar->acc = spar->dynamic(msys, spar);
		}
		spar->calc_after_start(msys, spar);
		spar->meas	= 0;
	}

	spar->final_calc(msys, spar);
	spar->print_result(spar->value);

	return 0;

}

//----------
//Initialization functions:
//----------

int scan_system_conf (mSystem *msys, eParams *epar, int argc, char *argv[])
{
    	switch ( system_type_choice (argv[1]) )
	{
		case cl: if ( scan_conf_cl( &(msys->R_gs), &(msys->flag), &(msys->num_shells), &(msys->sh_size), epar) )
				return 1;
			 break;

		case cr: if (scan_conf_cr( &(msys->R_gs), &(msys->nx), &(msys->ny), epar) )
				return 1;
			 break;

		default: printf("Unknown system type.\n");
			 return 1;
	}

	return 0;
}

int 	sParams_init (sParams *spar, int argc, char *argv[], int num_part, eParams *epar)
{
        spar->method_flag	= method_choice		(argv[2]);
        
	if ( spar->method_flag < 0 || sscanf(argv[4], "%lf", &(spar->T)) == 0 || spar->T < 0 )
	{
		printf("sParams_init: \t cannot scanf method, value or temperatura. \n");
		return 1;
	}

	if (( argc == 6 && strcmp(argv[5], "-t") == 0) || ( argc == 8 && strcmp( argv[7], "-t" ) == 0 ) )
		spar->time_flag = 1;
	else 
		spar->time_flag = 0;

        spar->rand 		= gsl_rng_alloc(gsl_rng_mt19937);
	spar->epar		= epar;
        spar->fid_T		= ACCURACY_T;
        spar->fid_p		= ACCURACY_p;
	spar->period_mc		= PERIOD * num_part;

        spar->max_dev     	= MAX_DEV;
        spar->max_vel     	= MAX_VEL;
        spar->now_shift		= 0;
        spar->meas  		= 0;
	spar->acc 		= 0;

	spar->shift_part_meas	= ceil(num_part*PER_SHIFT/100);

	switch( spar-> method_flag )
	{
		case md:
			spar->dynamic 		= dynamic_md;
			spar->dynamic_init	= dynamic_init_md;

			switch ( epar->sys_type )
			{
				case cl: 
					spar->trial_steps	= TRIAL_STEPS_MD_CL;
					spar->steps	 	= STEPS_MD_CL;
					spar->starts	 	= STARTS_MD_CL;
					break;
				case cr: 
					spar->trial_steps	= TRIAL_STEPS_MD_CR;
					spar->steps	 	= STEPS_MD_CR;
					spar->starts	 	= STARTS_MD_CR;
					break;
				default: 
					printf("sParams_init: \t unknown system type.\n");	
					return 1;
			}
			break;

		case mc:
			if ( spar->T == 0 ) 
			{ 
				printf ("sParams_init: \t in MC temperatura cannot be 0. \n"); 
				return 1; 
			}

			spar->dynamic 		= dynamic_mc;
			spar->dynamic_init	= dynamic_init_mc;
			
			switch ( epar->sys_type )
			{
				case cl: 
					spar->trial_steps	= TRIAL_STEPS_MC_CL;
					spar->steps	 	= STEPS_MC_CL;
					spar->starts	 	= STARTS_MC_CL;
					break;

				case cr: 
					spar->trial_steps	= TRIAL_STEPS_MC_CR*num_part;
					spar->steps	 	= STEPS_MC_CR*num_part;
					spar->starts	 	= STARTS_MC_CR*num_part;
					break;
				default: 
					printf("sParams_init: \t unknown system type.\n");	
					return 1;
			}
			break;

		default: 
			printf("sParams_init: \t unknown method.\n"); 
			return 1;
	}
	return 0;
}

int mSystem_init (mSystem *msys, int argc, char *argv[], sParams *spar)
{
	int 	i, size = msys->R_gs->size;

	msys->now_part	= 0;
	msys->s[0]	= -1;
	msys->s[1]	= -1;

	msys->num_part	= size/2;
	msys->H		= pot_energy(msys->R_gs, spar->epar);

	msys->R		= gsl_vector_alloc (size);
	msys->f		= gsl_vector_alloc (size);
	msys->p		= (double *) 	calloc(sizeof(double), size);

	if ( spar->method_flag == mc )	

		msys->r		= gsl_vector_alloc (size);

	switch( spar->epar->sys_type )
	{
		case cl:

			msys->sort		= (double *) 	calloc(sizeof(double), 	msys->num_part);
			msys->order 		= (int *) 	calloc(sizeof(int),    	msys->num_part);
			msys->sh_of_part	= (int *) 	calloc(sizeof(int),  	msys->num_part);
			msys->part_in_sh	= (int **) 	calloc(sizeof(int*), 	msys->num_shells);
	
			for( i = 0; i < msys->num_shells; i++ )
				msys->part_in_sh[i] = (int *) calloc(sizeof(int), msys->sh_size[i]);

			if ( argc > 6 )
				if ( sscanf(argv[5], "%d", &(msys->s[0])) == 0 || sscanf(argv[6], "%d", &(msys->s[1])) == 0 )
				{
					printf("mSystem_init: \t cannot scan numbers of considered shells.\n");
					return 1;
				}

			msys->s[0]--;
			msys->s[1]--;

			if ( msys->s[0] > msys->num_shells || msys->s[1] > msys->num_shells) 
			{
				printf("mSystem_init: \t bad considered shells.\n");
				return 1;
			}

			msys->nx = 0;
			msys->ny = 0;

			break;

		case cr:
			msys->sort		= NULL;
			msys->sh_of_part	= NULL;
			msys->order	 	= NULL;
			msys->part_in_sh	= NULL;
	
			break;

		default:
			printf("mSystem_init: \t unknown system type.\n");
			return 1;
		
	}

	return 0;
}

int value_init( mSystem *msys, int argc, char *argv[], sParams *spar )
{
	int flag = 1;

	if ( ( spar->value_flag=value_flag_choice(argv[3]) ) < 0)
		return 1;

	switch( spar->value_flag )
	{
		case temp: 	flag = temp_init    (msys, spar);	break;
		case fenergy:	flag = fenergy_init (msys, spar);	break;
		case penergy:   flag = penergy_init (msys, spar);	break;
		case delta:	flag = delta_init   (msys, spar);	break;
		case orient:	flag = orient_init  (msys, spar);	break;
		case trajs: 	flag = trajs_init   (msys, spar);	break;
		case rho:	flag = rho_init     (msys, spar);	break;

		default:	printf("value_choice: \t Unknown value.\n");
				return 1;
	}

	if ( flag )	
		return 1; 

	return 0;
}



