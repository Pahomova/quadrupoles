#ifndef MY_ENERGY
#define MY_ENERGY

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <complex.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_vector.h>

//----------
// Type of simulated system
//----------

enum system_type { cl /*cluster*/ , cr /*crystal*/ };
typedef enum system_type System_type;  

static inline System_type system_type_choice(char str[])
{
	if ( strcmp (str, "cluster") == 0 || strcmp (str, "cl") == 0 )
        	return cl;
	if ( strcmp (str, "crystal") == 0 || strcmp (str, "cr") == 0)
        	return cr;
	return -1;
}

//----------
// Potential energy parametres
//----------

typedef struct 	energy_params eParams;

struct  energy_params
{
	int 		type_part; 					// particles type
	System_type	sys_type;					// system type
	double 		size_x,						// x cell size for crystal
			size_y,						// x cell size for crystal
			size_x2,					// x cell size/2 for crystal
			size_y2,					// x cell size/2 for crystal
			Rc;						// cutoff radius
									
	double 		(* pow)		(double); 			// function (x)^type_part
	double 		(*pow2)		(double);			// function (x)^{type_part+2}
	double 		(*ex_pot)	(double, double);		// external potenrial
	double 		(*dis)		(double, double, double);	// function return distance between two particlaen on the direction 


};

//----------
//Functions to potential energy
//----------

double		pot_energy		(const gsl_vector * R, void * Epar);	
					// pptential energy of system
double 		pot_energy_1p		(const gsl_vector * R, void * Epar, int i);	
					// potential energy of 1 particle			
void 		grad_of_pot_energy 	(const gsl_vector * R, void * Epar, gsl_vector * grad); 
					// gradient of potential energy
void 		pot_energy_grad 	(const gsl_vector * R, void * params, double *U, gsl_vector *grad);
					// pot_energy + grad_of_pot_energy	

//----------
//Functions to kinetic energy
//----------

double 		kin_energy  		( double *p, int num_part); 
					// kinetic energy of system
double 		temperatura 		( double *p, int num_part);	
					// temperatura of system

//----------
//Auxiliary functions
//----------

int 		eParams_init 	( eParams *epar, int type_part, int sys_type, double size_x, double size_y, double Rc);
				// initialization of eParams
void 		gsl_vector_rng 	( gsl_vector *x, gsl_rng *rand, double size);
				// filling a gsl_vector with random values in the range [-size; size]


static inline double  	randpoint               (double size, gsl_rng *rand)		{ return 2*size*(0.5-gsl_rng_uniform(rand)); }
						// random value in the range [-size; size]
static inline void 	gsl_vector_element_add  (gsl_vector *x, size_t i, double y) 	{ gsl_vector_set(x, i, gsl_vector_get(x, i)+y );}
						// adding y to i-element of gsl_vector x
static inline void 	swap			(void *X, void *Y) 	
{ 
	size_t *x=(size_t *)X, *y=(size_t *)Y, buf=*x; 

	*x =*y; 
	*y = buf; 
}
#endif
