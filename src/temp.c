#include "temp.h"

struct value_temp
{
        int     start;
        double  *T, aT, sT;
};
typedef struct value_temp Temp;

int     temp_init  (mSystem *msys, sParams * spar)
{

	Temp *temp;

	temp  	= (Temp *) 	calloc (sizeof(Temp),   1);
	temp->T	= (double *) 	calloc (sizeof(double), spar->starts);

	spar->value             = (void *)temp;
	spar->calc_after_step   = temp_after_step;
	spar->calc_after_start  = temp_after_start;
	spar->final_calc        = temp_final_calc;
	spar->print_result      = temp_print;

	return 0;
}

void    temp_after_step (mSystem * msys, sParams *spar)
{
	Temp *temp = (Temp *) spar->value;

	temp->T[temp->start]	+= temperatura(msys->p, msys->R->size/2);
}

void    temp_after_start (mSystem * msys, sParams *spar)
{
	Temp *temp = (Temp *) spar->value;

	temp->T[temp->start] 	/= spar->steps;
	temp->aT		+= temp->T[temp->start];
	temp->start ++;

}

void    temp_final_calc (mSystem * msys, sParams *spar)
{
	int i;
	Temp *temp = (Temp *) spar->value;

	temp->aT	/= spar->starts;
	for ( i = 0; i < spar->starts; i++ )
		temp->sT += gsl_pow_2(temp->T[i]-temp->aT);

	temp->sT = sqrt(temp->sT/(spar->starts-1));
}

void    temp_print (void *value)
{
	int i;
	Temp *temp = (Temp *) value;

	printf("%lf \t %lf\n", temp->aT, temp->sT);

	for ( i = 0; i < temp->start ; i++ )
		printf("%lf \n", temp->T[i]);
}

