#include "mc.h"

int     dynamic_mc_all	( mSystem *msys,  sParams *spar);
			// mc-dynamic with shifting of all particles in system
int     dynamic_mc_1p	( mSystem *msys,  sParams *spar);
			// mc-dynamic with shifting of  only one particle

void 	new_conf_mc	( gsl_vector *R, gsl_vector *R_old, double *p, sParams *spar);
			// create of new configuration
void    conf_correction ( gsl_vector *R, double *p, sParams *spar);
			// "velocity correction" to mc method


int  dynamic_init_mc  ( mSystem *msys, sParams *spar)
{
        int     i;
        double  acc = 0;

        while ( acc < 0.35 || acc > 0.4 )
        {
                spar->max_dev *= 1+(acc - 0.4);
                acc 	= 0;

		new_conf_mc(msys->R, msys->R_gs, msys->p, spar);
		msys->H = pot_energy(msys->R, spar->epar);

                for( i = 0; i < spar->trial_steps; i++ )
                        acc += dynamic_mc(msys, spar);

                acc /= spar->trial_steps;
        }
        return 0;
}

//----------
//Dynamic fuctions
//----------

int     dynamic_mc     ( mSystem *msys,  sParams *spar)
{
	int acc;

	if ( spar->now_shift == spar->period_mc )
	{
		acc 		= dynamic_mc_all(msys, spar);
		spar->now_shift	= 0;
	}
	else
	{
		acc = dynamic_mc_1p(msys, spar);
		spar->now_shift ++;
	}

	return acc;
}

int     dynamic_mc_all     ( mSystem *msys,  sParams *spar)
{
	double 		a, h;

	new_conf_mc(msys->r, msys->R, msys->p, spar);

	h = pot_energy(msys->r, spar->epar);
	a = exp((msys->H-h)/spar->T);

	if ( (a >= 1) || (a > gsl_rng_uniform(spar->rand)) )
	{
		swap(msys->R, msys->r);	
		msys->H = h;
		return 1;
	}

	if ( spar->epar->sys_type == cr )
		re_allocation(msys->R, spar->epar->size_x, spar->epar->size_y);

	return 0;
}

int     dynamic_mc_1p        ( mSystem *msys,  sParams *spar)
{
	int 		ix, iy, acc;
	double 		a, h, dx, dy;

	ix 	= 2*msys->now_part;
	iy 	= 2*msys->now_part+1;
	h  	= pot_energy_1p(msys->R, spar->epar, msys->now_part);

	dx = randpoint(spar->max_dev, spar->rand);
	dy = randpoint(spar->max_dev, spar->rand);

	gsl_vector_element_add(msys->R, ix, dx);
	gsl_vector_element_add(msys->R, iy, dy);

	h -= pot_energy_1p(msys->R, spar->epar, msys->now_part);
	a  = exp(h/spar->T);

	if ( (a >= 1) || (a > gsl_rng_uniform(spar->rand)) )
	{
		msys->H 	 -= h;
		msys->p[ix]	 = dx;
		msys->p[iy]	 = dy;
		acc		 = 1;
	}
	else
	{
		gsl_vector_element_add(msys->R, ix, -dx);
		gsl_vector_element_add(msys->R, iy, -dy);

		msys->p[ix]	 = 0;
		msys->p[iy]	 = 0;
		acc		 = 0;
	}

	if ( msys->now_part == msys->num_part - 1 )
	{
		conf_correction (msys->R, msys->p, spar);
		msys->now_part = 0;
	}
	else 
		msys->now_part++ ;

	if ( acc && spar->epar->sys_type == cr )
	{
		gsl_vector_set(msys->R, ix, remainder(gsl_vector_get(msys->R, ix ), spar->epar->size_x));
		gsl_vector_set(msys->R, iy, remainder(gsl_vector_get(msys->R, iy ), spar->epar->size_y));
	}

	return acc;
}

//----------
//Auxiliary functions
//----------

void new_conf_mc(gsl_vector *R, gsl_vector *R_old, double *p, sParams *spar)
{
	int i;

	new_conf (R, R_old, spar->rand, spar->max_dev);

        for( i = 0 ; i < R->size; i++)
		p[i] = gsl_vector_get(R, i) - gsl_vector_get(R_old, i) ;

        velocity_correction (R_old, p, spar->method_flag, spar->fid_p);

        for( i = 0 ; i < R->size; i++)
		gsl_vector_set(R, i, gsl_vector_get(R_old, i) +p[i]) ;
}

void    conf_correction (gsl_vector *R, double *p, sParams *spar)
{
	int 	i;

        for( i = 0 ; i < R->size; i++)
		gsl_vector_element_add(R, i, -p[i]);

        velocity_correction (R, p, spar->method_flag, spar->fid_p);

        for( i = 0 ; i < R->size; i++)
		gsl_vector_element_add(R, i, p[i]);
}








