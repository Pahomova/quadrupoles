#ifndef DELTA
#define DELTA

#include "value.h"

int     delta_init 		(mSystem *, sParams *);
void    delta_after_step 	(mSystem *, sParams *);
void    delta_after_start 	(mSystem *, sParams *);
void    delta_final_calc 	(mSystem *, sParams *);
void    delta_print		(void *);

#endif
