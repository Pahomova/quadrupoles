#include "penergy.h"

#define PERIOD_FE 1

struct value_penergy
{
	int 	i;
	double 	H;
};
typedef struct value_penergy Penergy;

int     penergy_init  (mSystem *msys, sParams * spar)
{
	Penergy	*penergy;

	penergy		= (Penergy *) calloc (sizeof(Penergy), 1);;
	penergy->i	= 0 ;
	penergy->H	= 0 ;

	spar->value		= penergy;
	spar->calc_after_step   = penergy_after_step;
	spar->calc_after_start  = penergy_after_start;
	spar->final_calc        = penergy_final_calc;
	spar->print_result      = penergy_print;
	spar->steps	    	= PERIOD_FE*1000;
	spar->starts	    	= 1;
	return 0;
}

void    penergy_after_step (mSystem * msys,  sParams *spar )
{
	Penergy	*penergy = (Penergy *) spar->value;

	if ( penergy->i%PERIOD_FE == 0 )
		printf("%d %lf\n", (int)penergy->i/PERIOD_FE, pot_energy(msys->R, spar->epar));
	penergy->i++;
}

void    penergy_after_start (mSystem * msys,  sParams *spar ) 	{}	
void    penergy_final_calc (mSystem * msys,  sParams *spar )	{}
void    penergy_print (void *value)				{}

