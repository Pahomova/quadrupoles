#include "fenergy.h"

#define PERIOD_FE 1
struct value_fenergy
{
	long 	i;
	double 	E;
};
typedef struct value_fenergy Fenergy;

int     fenergy_init  (mSystem *msys, sParams * spar)
{
	Fenergy	*fenergy;

	fenergy		= (Fenergy *) calloc (sizeof(Fenergy), 1);;
	fenergy->i	= 0 ;
	fenergy->E	= 0 ;

	spar->value             = NULL;
	spar->calc_after_step   = fenergy_after_step;
	spar->calc_after_start  = fenergy_after_start;
	spar->final_calc        = fenergy_final_calc;
	spar->print_result      = fenergy_print;
	spar->steps	    	= PERIOD_FE*100;
	spar->starts	    	= 1;
	return 0;
}

void    fenergy_after_step (mSystem * msys,  sParams *spar )
{
	Fenergy	*fenergy = (Fenergy *) spar->value;
	if ( fenergy->i%PERIOD_FE == 0 )
		printf("%d %lf\n", (int)fenergy->i/PERIOD_FE, kin_energy(msys->p, msys->num_part)+pot_energy(msys->R, spar->epar));
	fenergy->i++;
}

void    fenergy_after_start (mSystem * msys,  sParams *spar ) 	{}	
void    fenergy_final_calc (mSystem * msys,  sParams *spar )	{}
void    fenergy_print (void *value)				{}

