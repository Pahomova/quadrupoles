#! /bin/bash


echo 'Таблица 1. Энергия и числа заполнения оболочек основных состояний.
\begin{longtable}{|c|c|c|c||c|c|c|c|}
\hline
N & Распределение & E& E/N &N & Распределение & E & E/N\\
 & по оболочкам & & &  & по оболочкам & &\\
\hline
\hline
\endhead '

echo $begin;

for x in `seq 2 50`
do
	y=`expr $x + 49` ;

	file1="res/cl/conf/${x}m.out" ;
	file2="res/cl/conf/${y}m.out" ;

	for a in 1 2 3
	do
		read shells1 ;
	done <$file1;

	for a in 1 2 3
	do
		read shells2 ;
	done <$file2 ;

	u=`./conf_energy <$file1`
	U1=` echo $u |  cut -d ' ' -f 2`;
	Un1=` echo $u |  cut -d ' ' -f 3`;
	u=`./conf_energy <$file2`
	U2=` echo $u |  cut -d ' ' -f 2`;
	Un2=` echo $u |  cut -d ' ' -f 3`;




	echo $x " & " $shells1 "&" ${U1:0:8} " & " ${Un1:0:8}" & ";
	echo "	" $y " & " $shells2 " & " ${U2:0:8} " & " ${Un2:0:8} '\\' ;
done

echo '\hline
\end{longtable}'

