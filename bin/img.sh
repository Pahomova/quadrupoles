#! /bin/sh

h_value="[ gconf_cl | gconf_cr | conf_energy | full_energy | delta | orient | mix]"
h_method="{-md|mc}"
h_stype="{-cl|-cr}"
h_num_part="{-n num_part}"
h_file="{-f filename}"
h_mono="{-mono}"
h_shells="{-s:num_s s1 s2 ... sn }"
h_tog="{ on one graphic : -tog }"
h_sh_img="{ shells imagine: -si}"
str_cr="Created file:"

help_str1="Use: \t $0 $h_value"
help_str2="Flags: \t  $h_method $h_stype $h_num_part $h_file $h_mono $h_shells $h_tog $h_sh_img"

if [ -z $1 ] || [ $1 = "-h" ]
then
	echo $help_str1
	echo $help_str2
	return
fi

value=$1
shift

while [ ! -z $1 ]
do
	case $1 in

		"-cl"|"-cr")
			sys_type=`echo $1 | sed s/'-'//g`
			shift ;;

		"-md"|"-mc")
			method=`echo $1|sed "s/-//g"`
			shift   ;;

		"-cl"|"-cr")
	 		sys_type=`echo $1|sed "s/-//g"`
			shift	;;

		"-n:"[0-9]*)
			num_part=`echo $1 | sed s/"-n:"/""/g`
			shift 	;;

		"-f2:"?*)
			file2=`echo $1 | sed s/"-f2:"/""/g`
			shift   ;;

		"-f:"?*)
			file=`echo $1 | sed s/"-f:"/""/g`
			shift   ;;

		"-s:"[0-9]*)
			if [ -z $1 ] ; then echo "Use: $h_shells" ; return ; fi
			num_s=`echo $1 | sed s/"-s:"/""/g`

			for x in `seq 1 $num_s`
			do
				if [ -z $2 ]
				then 	echo "Only few shells is present" ; break;
				else 	shift ; eval `echo s$x`=$1
				fi
			done
			shift ;;

		"-mono")
			mono=mono
			shift	;;

		"-tog")
		 	tog=tog
			shift	;;

		"-si")
			sh_img=sh_img
			shift	;;

		*)
			echo "Unknown flag: $1."
			shift ;;
	esac
done

case $value in

	"comp")	if [ -z "$file" ] || [ -z "$file2" ] ; then return; fi
		str_plot="plot \"res/$file\"  using 1:2:3 with errorbars pt 7 ps 0.3"
		str_plot="$str_plot , \"res/$file2\"  using 1:2:3 with errorbars pt 7 ps 0.3"
		file_pdf="res/comp.pdf"
		echo $file_pdf
		gnuplot <<END
set term pdf enhanced $mono
set output "$file_pdf"
set logscale x
set nokey
$str_plot
END

		;;

	"gconf_cr"|"gc_r")
		paths="res/cr/conf/"
		if [ -z "$file" ]
		then	files="${paths}*[0-9].out"
		else 	files="${paths}${file}.out"
		fi
		for file_now in $files
		do
			file_pdf=`echo $file_now | sed s/out/pdf/g`
			echo $str_cr $file_pdf
			gnuplot <<END
set term  pdf $mono size 2.3, 2.3 inches
set output "$file_pdf"
set nokey
set size square
plot "$file_now" w p ps 0.5
END
		done
		;;

	"gconf_cl"|"gc_l")
		paths="res/cl/conf/"

		if [ ! -z "$sh_img" ]
		then 	if [ -z "$num_part" ]
			then 	files=${paths}"*/"
			else	files=${paths}"${num_part}/"
			fi
		else	if [ -z "$file" ]
			then	if [ -z $num_part ]
				then 	files="${paths}*[0-9].out"
				else 	files="${paths}${num_part}.out"
				fi
			else 	files="${paths}${file}.out"
			fi
		fi
		for file_now in $files
		do
			if [ -z $num_part ]
			then 	str=`echo $file_now | sed 's/[^0-9]/ /g'`
				read num_part_now y << END
$str
END
			else 	num_part_now=$num_part;
			fi
		      	if [ $num_part_now -le 10 ]
			then size=2;
			elif [ $num_part_now -le 26 ]
      			then size=3;
     			elif [ $num_part_now -le 40 ]
      			then size=4;
			elif [ $num_part_now -le 80 ]
      			then size=5;
			else size=6;
			fi;


			if [ ! -z "$sh_img" ]
			then	for file_now1 in ${file_now}*[0-9].out
       				do
					if [ -z "$str_plot" ]
					then str_plot="plot \"${file_now1}\"  w p ps 0.5";
					else str_plot="${str_plot}, \"${file_now1}\"  w p  ps 0.5";
               				fi
					file_pdf=`echo $file_now | sed s/"\/*.out"/""/g `"${num_part_now}.pdf"
       				done
				gnuplot <<END
set term  pdf $mono size 2.3, 2.3 inches
set output "$file_pdf"
set nokey
set size square
set xrange [ -$size : $size ]
set yrange [ -$size : $size ]
$str_plot
END
			str_plot=""
			else	str_plot="plot \"$file_now\"  with points lt 3 pt 6"
				str_plot=$str_plot", \"$file_now\"  with points lt 3 pt 7 ps 0.2"
				file_pdf=`echo $file_now | sed s/out/pdf/g `
			fi
			echo $str_cr $file_pdf
			if [ -z "$sh_img" ]
			then	gnuplot <<END
set term  pdf $mono size 2.3, 2.3 inches
set output "$file_pdf"
set nokey
set size square
set xrange [ -$size : $size ]
set yrange [ -$size : $size ]
$str_plot
END
				fi
   			done
	;;

	"conf_energy"|"ce") 	if [ -z "$file" ] ; then files="res/conf_energy.out" ; else files="$res/${file}.out" ; fi
			 	file_pdf=`echo $files | sed s/out/pdf/g `
				echo $str_cr $file_pdf
				gnuplot << END
set term pdf $mono size 4, 2.5 inch
set output "$file_pdf"
set key right bottom
set xlabel "N"
set ylabel "U(N)/N"
f1(x) = a * ( x - 1 ) ** b
a = 1
b = 0.5
fit f1(x) "res/conf_energy.out" using 1:3 via a, b
str = sprintf("Фиттинг %f * (x-1) ** %f", a, b)
plot "$files" using 1:3 with points lt 3 pt 7 ps 0.3 title "Расчёт", f1(x) with lines lt 1 lw 2.5 title str
END
	;;


   	"delta"|"d")
		if [ -z "$method" ] ; then paths="res/cl/delta_*/" ; else paths="res/cl/delta_${method}"/; fi
		if [ -z "$file" ]
		then 	if [ -z $num_part ]
			then 	files="${paths}*.out"
			else	files="${paths}${num_part}.out"
			fi
		else	files="${paths}/${file}.out"
		fi

		for file_now in $files
		do
			file_pdf=`echo $file_now | sed s/out/pdf/g` ;

			echo $str_cr $file_pdf by $file_now

			if [ -z $num_part ]
			then	str=`echo $file_now | sed 's/[^0-9]/ /g'`
			read x y << END
$str
END
			title="Плавление кластера из $x атомов"
			fi

		gnuplot <<END
set term pdf enhanced $mono
set output "$file_pdf"
set logscale x
set key right bottom
set title "$title"
plot "$file_now"  using 1:2:3 with errorbars pt 7 ps 0.3 title "{/Symbol d}_r(T)"
END
	done 
	;;



	"o"|"orient")
 		if [ -z "$method" ] ; then paths="res/cl/orient_*/" ; else paths="res/cl/orient_${method}/" ; fi
		if [ -z "$file" ]
		then 	if [ -z $num_part ]
			then	if [ -z "$num_s" ]
				then 	files="${paths}*.out"
				else 	for x in `seq 1 $num_s`
					do
						files="${files} ${paths}*(`eval echo "\$"s$x`).out"
					done
				fi
				else 	if [ -z "$num_s" ]
					then 	files="${paths}${num_part}(*).out"
					else 	for x in `seq 1 $num_s`
						do
							s=`eval echo "\$"s$x`;
							files="${files} ${paths}${num_part}(${s}).out"
						done
					fi
				fi
				else	files="${paths}${file}.out"
					tog=""
			fi

			for file_now in $files
			do
				if [ -z "$tog" ] ; then file_pdf=`echo $file_now | sed s/out/pdf/g` ; fi
				if [ ! -z "`echo $file_now | grep '*'`" ] ; then continue ; fi

				if [ -z $num_part ]
				then	str=`echo $file_now | sed 's/[^0-9]/ /g'`
					read num_part_now y z << END
$str
END
					else num_part_now=$num_part
				fi

				title="Плавление кластера из $num_part_now атомов"
				title2="{|g_{$y}|(T)}"

				str=`echo $file_now | sed 's/[^0-9]/ /g'`
				read x y z << END
$str
END

				if [ -z "${tog}" ]
				then echo $str_cr $file_pdf
gnuplot <<END
set term pdf $mono enhanced
set output "$file_pdf"
set yrange [0:]
set logscale x
set key right bottom
set title "$title"
plot "$file_now"  using 1:2:3 with errorbars pt 7 ps 0.3 title "$title2" 
END
				else 	title2="${title2}_{$num_part_now}"
					if [ -z "$str_plot" ]
					then	str_plot="plot \"$file_now\"  using 1:2:3 with errorbars pt 7 ps 0.3 title \"$title2\""
					else 	str_plot="$str_plot, \"$file_now\"  using 1:2:3 with errorbars  pt 7 ps 0.3 title \"$title2\""
				fi
			fi

			done

			if [ ! -z "$tog" ]
			then 	if [ -z ${method} ]
				then file_pdf="res/orient_t"
				else file_pdf="${paths}"
				fi

				if [ -z "$file" ]
				then 	if [ ! -z "$num_part" ]
					then file_pdf="${file_pdf}${num_part}.pdf"
					else file_pdf="${file_pdf}test_orient.pdf"
					fi
				else 	file_pdf="${file_pdf}${file}.pdf"
				fi
				echo $str_cr $file_pdf
				gnuplot <<END
set term pdf $mono enhanced
set output "$file_pdf"
set yrange [0:]
set logscale x
set key right bottom
$str_plot
END

		fi

		;;


   	"mix"|"m")
 		if [ -z "$num_part" ] || [ -z "$num_s" ] ; then echo "Num_part? Shells?" ; return ; fi

		file_pdf="res/cl/mix/${num_part}.pdf"
		echo $str_cr $file_pdf
		par="using 1:2:3 with errorbars pt 7 ps 0.3 title"

		title="\{/Symbol d\}_r\(T\)"
		fin="${num_part}.out"
		str_plot="plot \"res/cl/delta_md/${fin}\" $par \"${title}_{md}\" axis x1y1"
		str_plot="$str_plot, \"res/cl/delta_mc/${fin}\" $par \"${title}_{mc}\" axis x1y1"

		for x in `seq 1 $num_s`
		do
			s_now=`eval echo "\$"s$x`
			fin="${num_part}($s_now).out"
			title="\{|g_\{${s_now}\}|(T)\}"
			str_plot="$str_plot, \"res/cl/orient_md/${fin}\" $par \"${title}_{md}\" axis x1y2"
			str_plot="$str_plot, \"res/cl/orient_mc/${fin}\" $par \"${title}_{mc}\" axis x1y2"
		done
gnuplot <<END
set term pdf $mono enhanced size 4.5, 3 inch
set output "$file_pdf"
set logscale x
set yrange [0:0.4]
set y2range [0:]
set xrange [0.0001:]
set key right bottom
set parametric
$str_plot
END



	;;


   	"fe"|"pe")
	 	if [ -z "$file" ] || [ -z "$sys_type" ]  ; then echo "File? Sys_type?"; return ; fi
		if [ "$value" = "pe" ] ; then ylab=E; else ylab=E; fi

		file="res/${sys_type}/${value}/${file}.out" 
	 	file_pdf=`echo $file | sed s/out/pdf/g `

			gnuplot <<END
set term pdf $mono enhanced
set output "$file_pdf"
set xlabel "T"
set ylabel "$ylab"
set nokey
set yrange [140:]
plot "$file"  using 1:2 with p  pt 7 ps 0.1
END


	;;

	"rho")
		
		if [ -z "$method" ] ; then paths="res/cr/rho_*/" ; else paths="res/cr/rho_${method}"/; fi
		if [ -z "$file" ] ; then files="${paths}*.out" ; else files="${paths}${file}.out" ; fi
			

		for file_now in $files
		do
		
			file_pdf=`echo $file_now | sed s/out/pdf/g` ;
			echo $str_cr $file_pdf by $file_now

		gnuplot <<END
set terminal pdf mono
set output "$file_pdf"
set nokey
set yrange [ 0:*]
plot "$file_now" w lp ps 0.1 pt 7
END
		done
	;;

esac

