#! /bin/sh

max_proc=4
time_sleep=60
type_part=5

h_value="[ help|delta|orient|mod_conf|conf_energy|gconf|fenergy ]"
h_stype="{-cl|-cr}"
h_method="{-md|-mc}"
h_num_part="{-n:num_part}"
h_shells="{-s:s1:s2}"
h_max_proc="{-mp:max_proc}"
h_time="{-ts:time_sleep}"
h_type_part="{-tp:type_part}"
h_T="{-T:T_global}"

str_help1="Use: \t $0 $h_value"
str_help2="Flags: \t $h_stype $h_method $h_num_part $h_shells $h_max_proc $h_time $h_type_part $h_T "
value=$1

if [ -z "$value" ] || [ "$value" = "help" ]
then	echo "$str_help1"; echo "$str_help2"; return ;
else 	shift
fi

while [ ! -z "$1" ]
do
	case $1 in
		"-md"|"-mc")	
			method=`echo $1 | sed s/'-'//g`
			shift ;;

		"-cl"|"-cr")	
			sys_type=`echo $1 | sed s/'-'//g` 
			shift ;;

		"-n:"[0-9]*)	
			num_part=`echo $1 | sed s/"-n:"/""/g` 
			shift ;;

		"-s:"[0-9]*":"[0-9]*)		
			str=`echo $1 | sed s/":"/" "/g`	
			read s s1 s2 << END
$str
END
			shift ;;

		"-mp:"[0-9]*)
			max_proc=`echo $1 | sed s/"-mp:"/""/g` 
			shift;;

		"-ts:"[0-9]*)
			time_sleep=`echo $1 | sed s/"-ts:"/""/g` 
			shift ;;

		"-tp:"[0-9]*)
			type_part=`echo $1 | sed s/"-tp:"/""/g` 
			shift ;;

		"-T:"[0-9]*)		
			T_global=`echo $1 | sed s/"-T:"/""/g` 
			shift ;;

		*) 	echo "Unknown flag: $1."
			shift ;;
	esac

done

case "$value" in

	"fenergy"|"fe") 	

		if [ -z "$method" ] || [ -z "$sys_type" ] || [ -z "$T_global" ]; then echo "Method? Sys_type? Temperatura?"; return; fi

		case "$sys_type" in
			"cl")	if [ -z "$num_part" ] ; then echo "Num_part?"; return; fi
				file_in="res/cl/conf/${num_part}m.out"
				file_out="res/cl/fe/${num_part}.out"
				;;
	
			"cr")	if [ -z "$file" ] ; then echo "File in?"; return; fi
				file_in="res/cr/conf/${file}.out"
				file_out="res/cr/fe/${file}.out"
				;;
		esac

		./value $sys_type $method fenergy $T_global < $file_in >  $file_out &
		echo "#" $file_out : $file_in
		;;
 
   	"delta"|"orient"|"rho")

		if [ -z "$method" ] || [ -z "${num_part}" ] ; then echo "Method? Num_part?" ; return ; fi
		case "$value" in
			"delta") 
				sys_type=cl
				s1="" 
				s2="" 
				file_out="res/${sys_type}/${value}_${method}/${num_part}.out"
				echo "#" $file_out
				;;

			"orient")
				sys_type=cl
				if [ -z "$s1" ] || [ -z "$s2" ]
				then 	echo $h_shells
				return
				fi
				file_out="res/${sys_type}/${value}_${method}/${num_part}(${s1}${s2}).out"
				echo "#" $file_out
				;;
			"rho")
				s1=""
				s2=""
				sys_type=cr
		esac

		file_in="res/${sys_type}/conf/${num_part}m.out"
		echo "min max dT";

		while read min max dT
		do
		   	if [ $min = "q" ]
		   	then 	break;
		   	elif [ -z $dT ]
		   	then	dT=1;
		      		max=$min;
		   	fi;

		   	T=$min;
			a=`echo " scale = 9; $T <= $max" | bc` ;

		   	while [ 1 -eq $a ]
		   	do
		   	   	T_result="$T_result $T";
		   	   	T=`echo "scale = 9; $T + $dT" | bc`;
		   	   	a=`echo " scale = 9; $T <= $max" | bc` ;
		   	done
		done

		echo $T_result;

		for T in $T_result
		do
			T_now=$T;
			while [ "$T_now" = "$T" ]
			do
				acc=;
				X=`ps|grep -c "value"`;
				Y=`ps|grep -c "value.sh"`;
				X=`expr $X - $Y`;
				X=`expr $max_proc - $X`;

				if [ $X -gt 0 ]
				then
					if [ ${value} = "rho" ] ; then file_out="res/$sys_type/rho_${method}/T=${T_now}.out" ; fi
				      	./value ${sys_type} $method $value $T_now $s1 $s2 < $file_in >>  $file_out&
					echo "#" $file_out : $T_now;
					T_now=;
				else
					sleep $time_sleep ;
				fi
			done
		done

		;;

	"mod_conf"|"conf_energy"|"ce"|"gconf")

		if [ "$value" = "conf_energy" ] || [ "$value" = "ce" ] ; then file_out="res/conf_energy.out" ; rm -f "$file_out" ; fi
		echo "[min] [max]"
		while read min max
		do
	   		if [ $min = "q" ]
	   		then	break;
	   		elif [ -z $min ]
	   		then 	max=$min;
	   		fi
			num_part="${num_part} "`seq $min $max`
		done 

		echo $num_part

		for num_part_now in $num_part
		do
			case $value in
				"mod_conf")
					file_in="res/cl/conf/${num_part_now}.out"
					file_out="res/cl/conf/${num_part_now}m.out"
      					./shells $num_part_now < $file_in  >> $file_out &
					echo "#" $file_out : $file_in
				;;
				"conf_energy"|"ce")
					file_in="res/cl/conf/${num_part_now}m.out"
   		   			./conf_energy < ${file_in} >> ${file_out} &
					echo "#" $file_out : $file_in
				;;
				"gconf")
					file_out="res/cl/conf/${num_part_now}.out"
					./gconf $type_part $num_part_now > ${file_out} &
					echo "#" $file_out : $file_in
				;;
			esac
   		done
		;;
esac
