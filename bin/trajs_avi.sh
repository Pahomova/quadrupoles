#!/bin/sh

if [ -z $3 ]
then
   echo "[num_part] [file_in] [file_name_out] [type_part_name]"
   return;
fi

argc () { echo $#; };

file1="res/trajs/${2}.out";
num_part=$1;
num_x=`expr $1 + $1`;


rm res/trajs/img/*
rm res/trajs/coords/*


n=1;
m=0;

while read x y
do
	if [ -z $y ]
	then
		break;
	fi;

	if [ $m = $num_part ]
	then
		n=`expr $n + 1`;
		m=0;
	fi;

	echo $x $y >> "res/trajs/coords/$n";
	m=`expr $m + 1`;

done < $file1


m=-1
p=$n

while [ $p -ne 0 ]
do
   p=`expr $p / 10 `
   m=`expr $m + 1`
done

k0=1
k=10
while [ $k -le $n ]
do
   o=
   for y in `seq 1 $m`
   do
     o=$o"0"
   done
   m=`expr $m - 1`
   k1=`expr $k - 1`
   for i in `seq $k0 $k1`
   do
    gnuplot << EOF
set nokey
set term png notransparent
set size square
set xrange [-3:3]
set yrange [-3:3]
set output "res/trajs/img/$o$i.png"
plot "res/trajs/coords/$i" with point lt 5 pt 7 ps 2
EOF
   done
   k0=$k
   k=`expr $k \* 10`
done

for i in `seq $k1 $n`
do
    gnuplot << EOF
set term png notransparent
set nokey
set size square
set xrange [-3:3]
set yrange [-3:3]
set output "res/trajs/img/$i.png"
plot "res/trajs/coords/$i" with point lt 5 pt 7 ps 2
EOF
done

mencoder "mf://res/trajs/img/*.png" -mf fps=24 -o "res/trajs/$2.mpeg" -ovc lavc -lavcopts vcodec=msmpeg4v2:vbitrate=800


